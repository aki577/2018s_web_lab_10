package ictgradschool.web.lab10.exercise01;

import java.io.Serializable;

public class UserInfo //implements Serializable
    {
    private String fName;
    private String lName;
    private String city;
    private String country;

    public UserInfo(String fName, String lName, String city, String country)
        {
        this.fName = fName;
        this.lName = lName;
        this.city = city;
        this.country = country;
        }

    public String getfName()
        {
        return fName;
        }

    public String getlName()
        {
        return lName;
        }

    public String getCity()
        {
        return city;
        }

    public String getCountry()
        {
        return country;
        }
    }
