package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet
    {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes
        out.println("<ul>");

        UserInfo user = new UserInfo(request.getParameter("fName"), request.getParameter("lName"), request.getParameter("city"), request.getParameter("country"));
        request.getSession(true).setAttribute("user", user);


        String fName = user.getfName();
        out.println("<li>First Name:" + fName + "</li>");
        String lName = user.getlName();
        out.println("<li>Last Name:" + lName + "</li>");
        String country = user.getCountry();
        out.println("<li>Country:" + country + "</li>");
        String city = user.getCity();
        out.println("<li>City:" + city + "</li>");
        out.println("</ul>");

        //could we use for?
//        request.getSession().setAttribute("attrFN", fName);
//        request.getSession().setAttribute("attrLN", lName);
//        request.getSession().setAttribute("attrCountry", country);
//        request.getSession().setAttribute("attrCity", city);

        out.println("<p>This is a list from session!</p>");
        out.println("<ul>");
        UserInfo ui = (UserInfo) request.getSession().getAttribute("user");
        out.println("<li>First Name: " + ui.getfName() + "</li>");
        out.println("<li>Last Name: " + ui.getlName() + "</li>");
        out.println("<li>Country: " + ui.getCountry() + "</li>");
        out.println("<li>City: " + ui.getCity() + "</li>");
        out.println(HtmlHelper.getHtmlPageFooter());

        //could we use for?
        Cookie cookieFN = new Cookie("cFN", fName);
        Cookie cookieLN = new Cookie("cLN", lName);
        Cookie cookieCountry = new Cookie("cCountry", country);
        Cookie cookieCity = new Cookie("cCity", city);
        response.addCookie(cookieFN);
        response.addCookie(cookieLN);
        response.addCookie(cookieCountry);
        response.addCookie(cookieCity);

        }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
        // Process POST requests the same as GET requests
        doGet(request, response);
        }
    }
