package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet
    {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
        {
        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {
            Cookie overwrite = new Cookie("hits", "0");
            overwrite.setMaxAge(0);
            resp.addCookie(overwrite);
            //TODO - add code here to delete the 'hits' cookie
//            for (Cookie co : req.getCookies()) {
//                if (co.getName().equals("hits")) {
//                    co.setMaxAge(0);}
//                    resp.addCookie(co);
//            }
        } else {
            //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie
            boolean foundCookie = false;
            for (Cookie co : req.getCookies()) {
                if (co.getName().equals("hits")) {
                    int num = Integer.parseInt(co.getValue());
                    num = num + 1;
                    co.setValue("" + num);
                    resp.addCookie(co);
                    foundCookie = true;
                    break;
                }
            }

            if (!foundCookie) {
                Cookie hits = new Cookie("hits", "1");
                resp.addCookie(hits);
            }

            //TODO - use the response object's send redirect method to refresh the page
            resp.sendRedirect("hit-counter.html");
        }
        }
    }
